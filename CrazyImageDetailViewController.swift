//
//  CrazyImageDetailViewController.swift
//  CrazyImage
//
//  Created by Apple on 29/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class CrazyImageDetailViewController: UIViewController {

    @IBOutlet weak var CrazyImageDetail: UIImageView!
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // detail image = image thumb selected
        CrazyImageDetail.image = image
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
