//
//  CrazyImageCollectionViewController.swift
//  CrazyImage
//
//  Created by Apple on 29/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

private let reuseIdentifier = "crazyImageCell"


class CrazyImageCollectionViewController: UICollectionViewController {

    
    /****************************
     * Instantiate FlikrService
     ***************************/
    let myFlkrServ = FlikrService.sharedInstance
    
//    let cell = collectionView().dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: IndexPath) as! CrazyImageCollectionViewCell
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false


        // Do any additional setup after loading the view.

        
        /**
         * Call all images
         */
        myFlkrServ.getAllPictures({ (request: URLRequest?, response: URLResponse?, error: Error?) -> Bool in
            if let response = response as? HTTPURLResponse, response.statusCode == 200 {
                print("DEBUG 'getAllPictures' : Enter To CALLBACK, IMGURLCOUNT = \(self.myFlkrServ.urlArray.count)" )
                DispatchQueue.main.async{
                    print("DEBUG  'getAllPictures' :  imgURLCount = \(self.myFlkrServ.urlArray.count)")
                    self.collectionView?.reloadData()
                }
                
                print("PRINT  'getAllPictures' = Response : \(response) \n")
            }

            return true
        })

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Navigation

    // -- Conf navigation application
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "segueDetailImageView" {
            if let cell = sender as? CrazyImageCollectionViewCell, let detailImageVC = segue.destination as? CrazyImageDetailViewController {
                
                    detailImageVC.image = cell.imageViewThumb.image
            }
        }
    }
    

    // MARK: UICollectionViewDataSource
    // -- Conf number of sections
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    // -- Conf number of images in each sections
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return myFlkrServ.urlArray.count
    }

        // -- Conf cell behavior
        // -- main function !!
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CrazyImageCollectionViewCell
    
        // Configure the cell
        
        // Retrieve URL -- TEST --
//        let url = URL(string: "https://dummyimage.com/100x100/123456/fff&text=lorem")
        
        /**
         * Iterate in urlArray to retrieve all url
         * and cast them to binanry.
         * Then, bind each cell with URLs in queue
         */
        
        if myFlkrServ.flkr != nil {
            print("DEBUG 'ControllerView file' INDEXPATH = \(indexPath.row)")
            
            let dummy = myFlkrServ.urlArray[indexPath.row]!
            
            print(" DEBUG 'ControllerView'= test dummy : \(dummy)")
            let data = try? Data(contentsOf: URL(string: dummy as! String)! )
                
            DispatchQueue.main.async {
                
                cell.imageViewThumb.image = UIImage(data: data!)
            }
            
        }// END if{}

        
        
        print("DEBUG 'ControllerView': - urlArray \(myFlkrServ.flkr)")
        
    
        return cell
    }

//    ||
//    ||
//    ||
//    ||
//    ||  Comm below
//    ||
//    ||
//    ||
//    ||
//    ||
//   \||/
//    \/
    
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override  collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
