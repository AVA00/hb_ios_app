//
//  Picture.swift
//  CrazyImage
//
//  Created by Apple on 29/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

struct Picture {
    let id: String
    let owner: String
    let secret: String
    let title: String
    let server: String
    let farm: Int
    let isfamily: Bool
    let ispublic: Bool
    let isfriend: Bool
}
