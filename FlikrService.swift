//
//  FlikrService.swift
//  CrazyImage
//
//  Created by Apple on 30/03/2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

class FlikrService {
    
    // API KEY
    private var API_KEY: String = ""
    
    
    public let baseUrl = "https://api.flickr.com/services/rest/?method=flickr.photos.search"
    
    public var urlArray = [Int:Any]()

    var flkr: Any? = nil
    
    // Instantiate singleton
    static let sharedInstance: FlikrService = FlikrService()
    
    // init session
    var sharedSession = URLSession.shared
    
    // private constructor
    private init() {
        /**
         * Retrieve api key in plist file
         * /!\ caution /!\
         * think to change dev api key every day!!
         */
        if let fileUrl = Bundle.main.url(forResource: "api_key", withExtension: "plist"),
            let data = try? Data(contentsOf: fileUrl) {
                if let result = try? PropertyListSerialization.propertyList(from: data, options: [], format: nil) as? [String:String] {
//                    print("API KEY : \(result?["key"]!)")
                    API_KEY = (result?["key"]!)!
            }
        }

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    /**
     * Search function to retrieve
     * pictures from tags
     */
    func getAllPictures(_ callback: @escaping (URLRequest?, URLResponse?, Error?) -> Bool ) -> Void {
        
        /* TODO : create a callback to reload view : cf completionHandler() function below */
        
        if let url = URL(string: "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(API_KEY)&tags=drones&sort=interestingness-desc&per_page=10&page=1&format=json&nojsoncallback=1") {
            // Transform url to request
            let request = URLRequest(url: url)
            
            // Create Data Task
            let dataTask = sharedSession.dataTask(with: request) {
                (data, response, error) in
                
                guard error == nil else {
                    print(error ?? "OH! OOOOOHH!....Ther some error here...")
                    return
                }
                
                guard let data = data else {
                    print("OH! OOOOOHH!!... It missing data ...")
                    return
                }
                
                // Retrieve root pics
                guard let jsonObj = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any] else {
                    print("Heu....ther are no pics here...")
                    return
                }
                
                // Retrieve pics
                guard let rootPics = jsonObj["photos"] as? [String: Any]else {
                    print("Hummm... there is no \'photos\' root property")
                    return
                }
                guard let pics = rootPics["photo"] as? [[String: Any]] else {
                    print("Hummm... there is no \'photos\' property")
                    return
                }
                
                // Instantiate object from response request
                
                self.flkr = pics.map {
                    Picture(
                        id: $0["id"] as! String,
                        owner: $0["owner"] as! String,
                        secret: $0["secret"] as! String,
                        title: $0["title"] as! String,
                        server: $0["server"] as! String,
                        farm: $0["farm"] as! Int,
                        isfamily: $0["isfamily"] as! Bool,
                        ispublic: $0["ispublic"] as! Bool,
                        isfriend: $0["isfriend"] as! Bool
                    )
                } // **end map()
                
                print("DEBUG : - pics \(pics)") // -- DUMP !!
//                print("DEBUG : - urlArray \(self.flkr)")    // -- DUMP !!
                
                self.buildUrl(self.flkr as! [Picture])
                print("\(self.urlArray)")    
                callback(request, response, error)
            }   // end dataTask()
            
            // Let's rollin' dataTaaaaaaassskk!!!!!
            dataTask.resume()
        }   // **end url
        
    }
    
    
    
    /**
     * Function to build images URLs
     * @return Dictionary urlArray
     */
    func buildUrl(_ pic: [Picture]) -> [Int:Any] {
        /**
         * 1- recupere l'objet Picture en fonction de son index ds le tableau flkr
         * 2- extraire les données requires
         * 3- constrire l'url
         */
        
//        let urlPic = "https://farm\(pic.farm).staticflickr.com/\(pic.server)/\(pic.id)_\(pic.secret)_t.jpg"
//        var i = 0
//        while urlArray[i] != nil {
//            i += 1
//        }
//        if urlArray[i] == nil {
//            urlArray[i] = urlPic
//        }
        for index in pic.enumerated() {
            
            // Retrieve index of element
            let pos = index.offset
            
            // build url with element properties
            let urlPic = "https://farm\(index.element.farm).staticflickr.com/\(index.element.server)/\(index.element.id)_\(index.element.secret)_n.jpg"
            
            // Put URLs to urlArray at index pos
            urlArray[pos] = urlPic
            
            print(" Print urlArray => \(urlArray)")
        }
        
        print("DEBUG : - urlArray registered = \(urlArray)")
        
        return urlArray     // return URL array with index
    }
    

}   // -- END FlikrService --
